# URLGet-method-in-Java


import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

public class URLGet {
	 
	  public static void main(String[] args)   throws IOException{
	   
	   // get command line arguments
	   if (args.length != 1)
	   {
		System.out.println("usage: java URLGet URL");
		System.exit(0);     
	   }
	   
	   // open connection
	   URL u = new URL(args[0]);
	   URLConnection connection = u.openConnection();
	   
	   // check whether response code is HTTP_OK (200)
	   
	   HttpURLConnection httpConnection = (HttpURLConnection)connection;
	   int code = httpConnection.getResponseCode();
	   if (code != HttpURLConnection.HTTP_OK)
	   {
		   String message = httpConnection.getResponseMessage();
		   System.out.println(code + " " + message);
		   return;
		}
		
		// read server response
		
		InputStream in = connection.getInputStream();
		BufferedReader reader = new BufferedReader(new InputStreamReader(in));
		
		boolean done = false;
		while (!done)
		{
			String input = reader.readLine();
			if (input == null) done = true;
			else System.out.println(input);
		}
	}
}			   
		   	  
